
# coding: utf-8

# # Executive Summary

# The case study consisted of EEG siginal data of 10 college students while they watched MOOC video clips. The videos consisted of content that was assumed not to be confusing for college students as well as videos that were on advanced topics and hence would be confusing for college students.. A total of 20 videos were prepared (10 from each category). Each video was 2 minutes long and the clips were chopped in the middle to make it more confusing. The data was collected with the help of a single-channel wireless MindSet that measures activity over the frontal lobe. After each session, students rated their confusion level on a scale of 1-7 which were futher normalized into labels of whether the student is confused or not. The data consisted of self-labelld and predefined labels. The self-labelled labels have been used as the target variable in this case study.
# 
# The case study followed the steps of data exploration, data preprocessing, modeling and finally the models were evaluated with the help of F1-Score. Feature selection techniques were also performed to improve the models but it was seen that all features used initally were important. Out of all the models tested, Decision Tree gave the best results which was further validated with the help of cross-validation

# # Exploratory Analysis

# In[176]:

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

dataset_EEG = pd.read_csv('EEG_data.csv')
dataset_Demographic = pd.read_csv('demographic_info.csv')
dataset_EEG.head()


# In[177]:

dataset_EEG.info()


# In[178]:

dataset_EEG['SubjectID'] = dataset_EEG['SubjectID'].astype(int)
dataset_EEG['VideoID'] = dataset_EEG['VideoID'].astype(int)
dataset_EEG['predefinedlabel'] = dataset_EEG['predefinedlabel'].astype(int)
dataset_EEG['user-definedlabeln'] = dataset_EEG['user-definedlabeln'].astype(int)
dataset_EEG.iloc[:, 2:].describe()


# In[225]:

# Pivoting the data to develop the by video and student.
data_pivotbyvideo = dataset_EEG.groupby(['SubjectID', 'VideoID'])['user-definedlabeln'].agg(lambda x: sum(x) > 0).unstack("VideoID")
data_pivotbyvideo


# In[226]:

fig = plt.figure(figsize=(15, 9))
plt.subplot(1, 2, 1)
data_pivotbyvideo.apply(sum).plot(kind='bar', title='Number of students confused by a video')
plt.subplot(1, 2, 2)
data_pivotbyvideo.apply(sum, axis=1).plot(kind='bar', title="Number of videos that confused a student")
plt.show()


# It seems Video 8 has confused most students. Student #4, #5 and #7 seem to be the most confused.

# # Data Preprocessing

# In[181]:

from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import seaborn as sns
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz


# In[182]:

dataset = pd.read_csv('EEG_data.csv')


# In[183]:

dataset.head()


# In[184]:


df = pd.DataFrame(dataset)
df.tail()


# In[185]:

#Checking if there are any null values
df.isnull().values.any()


# In[186]:

df.shape
df.head()


# # Checking for any inconsistencies in data

# In[232]:

df['Attention'].plot()
plt.show()


# In[188]:

df['Raw'].plot()
plt.show()


# In[189]:

df['Mediation'].plot()
plt.show()


# There appears to be some inconsistency or bad data that needs to be removed. It seems that there some values that are 0.

# In[190]:

df.groupby(['SubjectID', 'VideoID']).filter(lambda x: x['Attention'].sum()==0).groupby(['SubjectID', 'VideoID']).size()


# In[191]:

df.groupby(['SubjectID', 'VideoID']).filter(lambda x: x['Mediation'].sum()==0).groupby(['SubjectID', 'VideoID']).size()


# Student 6 and 3 for specific movie 3 have problems. They contain errors and should be removed.
# 
# 

# In[192]:

dataset_clean = df.query('(SubjectID != 6) & (SubjectID != 3 | VideoID !=3)')
dataset_clean.shape


# In[193]:

dataset_clean.reset_index()['Attention'].plot()
plt.show()


# ### Outlier Detection

# In[270]:

from pylab import *
points = boxplot(dataset_clean['Raw'], showfliers=True, vert=False)
plt.show()


# In[271]:

len(points['fliers'][0].get_ydata())


# Since, all the features have showed more than 5% of the data as outliers, the decision was taken to keep them. This can be further analysed in sensitivity analysis to see the performance that might get affected by these points. However, since it is more than 5% of the data, it seems these are valid points.

# ### Check for Correlation

# In[194]:

dataset_clean.head()


# In[195]:

#Checking for correlation following. Remove one of the variables if two are correlated with more than 0.9 (Based on Abott 2014)
import matplotlib.pyplot as plt
features_Useful = ['Attention','Mediation','Raw','Delta','Theta','Alpha1','Alpha2','Beta1','Beta2','Gamma1','Gamma2']
corr = dataset_clean[features].values
corr = pd.DataFrame(corr)

print(corr.corr())



# In[196]:


plt.matshow(corr.corr())
plt.show()




# There is correlation however, since there is no correlation above 0.9, there is no need to remove any features based on (Abott 2014). 

# # Modeling before feature selection
# 
# 

# In[215]:

dataset_clean['user-definedlabeln'].value_counts()


# The class is fairly balanced and hence there is no need to apply any sampling methods.

# ### Decision Tree

# In[197]:

#Make Test train splits
from sklearn.cross_validation import train_test_split
#User-DefinedLabel will be used as the target variable
y2 = dataset_clean['user-definedlabeln']
featuresForModeling = ['Attention','Mediation','Raw','Delta','Theta','Alpha1','Alpha2','Beta1','Beta2','Gamma1','Gamma2']
X = dataset_clean[featuresForModeling]
X_train, X_test, y_train, y_test = train_test_split(X, y2, test_size = 0.25, random_state = 0)

# Feature Scaling as the dataset contains a range of values
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

#Decision Tree
from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion = 'entropy', max_depth=2)
classifier.fit(X_train, y_train.values.ravel())

y_pred = classifier.predict(X_test)
# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
print(f1_score(y_test, y_pred, average='binary'))
print(precision_score(y_test, y_pred, average='binary'))
print(recall_score(y_test, y_pred, average='binary'))


# In[198]:

import graphviz 
from sklearn.tree import export_graphviz
tree_view = export_graphviz(classifier, 
                            out_file=None, 
                            feature_names = featuresForModeling,
                            class_names = ['No confused', 'Confused'])  
tree1viz = graphviz.Source(tree_view)
tree1viz


# From the tree, Attention, Gamma2 and Theta are good indicators to tell if a student is confused.

# ### Logistic Regression

# In[199]:

#Fitting Logistic Regression

from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state = 0)
classifier.fit(X_train, y_train.values.ravel())
# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
#from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)
print(f1_score(y_test, y_pred, average='binary'))
print(precision_score(y_test, y_pred, average='binary'))
print(recall_score(y_test, y_pred, average='binary'))



# ### SVM 

# In[200]:

# Fitting SVM to the Training set
from sklearn.svm import SVC
classifier = SVC(kernel = 'linear', random_state = 0)
classifier.fit(X_train, y_train.values.ravel())
# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)
print(f1_score(y_test, y_pred, average='binary'))
print(precision_score(y_test, y_pred, average='binary'))
print(recall_score(y_test, y_pred, average='binary'))


# # Including Demographic Data and checking for any increase in F-score
# 

# In[201]:


dataset_Demographics = pd.read_csv('demographic_info.csv')
dataset_Demographics.columns = ['subject ID', 'age', 'ethnicity', 'gender']
dataset_Demographics.head()


# In[202]:


dataset_Demographics.columns = ['subject ID', 'age', 'ethnicity', 'gender']
dataset_merged = df.merge(dataset_Demographics, left_on="SubjectID", right_on="subject ID")
dataset_merged.head()


# In[203]:

#Ethinicity and Gender are categorical variables and hence giving them numbers
#dataset_merged["ethnicity"]
dataset_merged['ethnicity'] = dataset_merged['ethnicity'].astype("category").cat.codes
dataset_merged['gender'] = dataset_merged['gender'].astype("category").cat.codes
dataset_merged.head()


# In[204]:

columns = ['SubjectID', 'VideoID' ,'Attention','Mediation','Raw','Delta','Theta','Alpha1','Alpha2','Beta1','Beta2','Gamma1',
            'Gamma2', 'predefinedlabel', 'user-definedlabeln','subject ID', 'age', 'ethnicity', 'gender']
feature_forModel = ['Attention','Mediation','Raw','Delta','Theta','Alpha1','Alpha2','Beta1','Beta2','Gamma1',
            'Gamma2', 'age', 'ethnicity', 'gender']


dataset_merged = dataset_merged.reindex(columns=columns)


X = dataset_merged[feature_forModel]
y_userdefinedlabel = dataset_merged['user-definedlabeln']


# ### Decision Tree

# In[205]:

#Modeling to see if the additional features help

#Make Test train splits
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y_userdefinedlabel, test_size = 0.25, random_state = 0)

# Feature Scaling as the dataset contains a range of values
#from sklearn.preprocessing import StandardScaler
#sc = StandardScaler()
#X_train = sc.fit_transform(X_train)
#X_test = sc.transform(X_test)

#Decision Tree
from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion = 'entropy', max_depth=2)
classifier.fit(X_train, y_train)

y_pred = classifier.predict(X_test)
# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
print(f1_score(y_test, y_pred, average='binary'))
print(precision_score(y_test, y_pred, average='binary'))
print(recall_score(y_test, y_pred, average='binary'))


# In[206]:

import graphviz 
from sklearn.tree import export_graphviz
tree_view = export_graphviz(classifier, 
                            out_file=None, 
                            feature_names = feature_forModel,
                            class_names = ['No confused', 'Confused'])  
tree1viz = graphviz.Source(tree_view)
tree1viz


# In[207]:

from sklearn.model_selection import cross_val_score
f1 = cross_val_score(classifier, X_train, y_train, cv=10, scoring='f1')
print(f1)


# From the tree, it looks like Delta and Attention are good indicators if the students are confused or not.

# ### Logistic Regression

# In[208]:

#Fitting Logistic Regression

#Make Test train splits
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y_userdefinedlabel, test_size = 0.25, random_state = 0)

# Feature Scaling as the dataset contains a range of values
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)



from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state = 0)
classifier.fit(X_train, y_train.values.ravel())
# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
#from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)
print(f1_score(y_test, y_pred, average='binary'))
print(precision_score(y_test, y_pred, average='binary'))
print(recall_score(y_test, y_pred, average='binary'))





#from sklearn.feature_selection import RFE
#from sklearn.svm import SVR
#estimator = SVR(kernel="linear")
#selector = RFE(estimator, 5, step=1)
#selector = selector.fit(X, y2.values.ravel())
#selector.support_ 
#selector.ranking_


# ### SVM

# In[209]:

# Fitting SVM to the Training set

#from sklearn.svm import SVC
classifier = SVC(kernel = 'linear', random_state = 0)
classifier.fit(X_train, y_train.values.ravel())
# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)
print(f1_score(y_test, y_pred, average='binary'))
print(precision_score(y_test, y_pred, average='binary'))
print(recall_score(y_test, y_pred, average='binary'))



# ## Feature Selection on merged set 

# ### PCA

# In[210]:

#dataset_merged.head()
#Performing Feature selection to see if it improves results
#Applying PCA
from sklearn.decomposition import PCA
pca = PCA(n_components = None)

X = pca.fit_transform(X_train)
explained_variance = pca.explained_variance_ratio_
print(explained_variance)
#No features can be removed with PCA as each feature exaplin very little variance



# It seems no components explain the majority of the variance and hence PCA cannot be used for feature selection.

# In[211]:


import matplotlib.pyplot as plt
import numpy as np

from sklearn.datasets import load_boston
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LassoCV


# We use the base estimator LassoCV since the L1 norm promotes sparsity of features.
clf = LassoCV()

# Set a minimum threshold of 0.25
sfm = SelectFromModel(clf, threshold=0.25)
sfm.fit(X_train, y_train)
n_features = sfm.transform(X).shape[1]

# Reset the threshold till the number of features equals two.
# Note that the attribute can be set directly instead of repeatedly
# fitting the metatransformer.
while n_features > 2:
    sfm.threshold += 0.1
    X_transform = sfm.transform(X)
    n_features = X_transform.shape[1]


# ### Forest of Trees 

# In[212]:

from sklearn.datasets import make_classification
from sklearn.ensemble import ExtraTreesClassifier

# Build a forest and compute the feature importances
forest = ExtraTreesClassifier(n_estimators=250,
                              random_state=0)

forest.fit(X_train, y_train)
importances = forest.feature_importances_
std = np.std([tree.feature_importances_ for tree in forest.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

for f in range(X.shape[1]):
    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

# Plot the feature importances of the forest
plt.figure()
plt.title("Feature importances")
plt.bar(range(X.shape[1]), importances[indices],
       color="r", yerr=std[indices], align="center")
plt.xticks(range(X.shape[1]), indices)
plt.xlim([-1, X.shape[1]])
plt.show()


# It seems that demographics dataset does not have much importance and this is something we already tested. Hence, it seems the EEG data carries the most importance for this classification task. As it can be seen from the graph, Attention is the most important feature.

# ## Results and Analysis 

# ### Without demographic

# In[213]:

from IPython.display import HTML, display
import tabulate
table = [ ["Classifier","F1-Score","Precision", "Recall"],
         ["Decision Tree", 0.60, 0.58, 0.60],
         ["Logistic", 0.63, 0.61, 0.66],
         ["SVM", 0.62, 0.61, 0.63]]
display(HTML(tabulate.tabulate(table, tablefmt='html')))


# ### With Demographic

# In[227]:

from IPython.display import HTML, display
import tabulate
table = [ ["Classifier","F1-Score","Precision", "Recall"],
         ["Decision Tree", 0.64, 0.63, 0.65],
         ["Logistic", 0.59, 0.59, 0.60],
         ["SVM", "0.59", "0.59", "0.58"]]
display(HTML(tabulate.tabulate(table, tablefmt='html')))


# It seems since SVM and logistic are linear models, with the addition of features, more variance is being added in these models. This can be attributed to the decrease in F1-Score. It was also seen from the forest feature selection technique that demographic features are not as important. Further, cross-validation was performed on decision tree to validate the increase in F1-score and it was seen that average F1-score accross all 10 folds was 0.62. Hence, the decision tree model can be said to be valid

# ## Summary

# To summarise this whole case study, first exploratory data analysis was performed to understand the data. Next, data cleaning step was performed to correct any noise that may give any problems during the classification phase. Correlation matrix was also developed after cleaning the inconsistencies but it was seen that there was no correlation strong enough to be removed. The cut-off point of 0.90 was taken and the max achieved was 0.65 between two features. The cut-off point was based on (Abott, 2014).
# Next classification models were built with and without demographics before feature selection. Next, feature selection techniques were performed but the result showed that the demographics features were not as important but rest of the features were important to classify and hence no further models were built.

# ### Further work 

# To go a step further, random forests and XGBoost could be tested. XGBoost could particularly help as it focuses on data points that have not given the correct classification.

# ## References

# Abbott, D., 2014. Applied predictive analytics: Principles and techniques for the professional
# data analyst. John Wiley & Sons.
